package org.sosy_lab.cpachecker.plugin.intellij.execution.cloud_execution;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.stream.Collectors;

class GetRequestMaker {
  String makeGetRequest(String urlToRead) throws IOException {
    URL url = new URL(urlToRead);
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    try {
      conn.setRequestMethod("GET");
      conn.addRequestProperty("Accept", "text/plain");
      try (BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
        return rd.lines().collect(Collectors.joining("\n"));
      }
    } finally {
      conn.disconnect();
    }
  }

  void downloadFile(String url, String pathToDownloadResult) throws IOException {
    HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
    try {
      conn.setRequestProperty("Accept", "application/zip");
      try (BufferedInputStream in = new BufferedInputStream(conn.getInputStream())) {
        File file = new File(pathToDownloadResult);
        Files.copy(in, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
      }
    } finally {
      conn.disconnect();
    }
  }
}
