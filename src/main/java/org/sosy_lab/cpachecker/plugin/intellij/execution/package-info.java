/*
 *  CPAchecker is a tool for configurable software verification.
 *  This file is part of CPAchecker Clion plugin.
 *
 *
 *
 *  CPAchecker web page:
 *    http://cpachecker.sosy-lab.org
 */
/** This package and its sub packages has implementation for executing plugin in IDE. */
package org.sosy_lab.cpachecker.plugin.intellij.execution;
