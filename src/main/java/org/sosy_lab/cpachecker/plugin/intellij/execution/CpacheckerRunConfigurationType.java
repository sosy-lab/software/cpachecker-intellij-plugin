package org.sosy_lab.cpachecker.plugin.intellij.execution;

import com.intellij.execution.configurations.ConfigurationFactory;
import com.intellij.execution.configurations.ConfigurationType;
import com.intellij.icons.AllIcons;
import javax.swing.Icon;
import org.jetbrains.annotations.NotNull;

public class CpacheckerRunConfigurationType implements ConfigurationType {
  /** this class is for creating configuration type for the run configuration ui. */
  @Override
  public String getDisplayName() {
    return "CPAchecker";
  }

  @Override
  public String getConfigurationTypeDescription() {
    return "In this configuration UI you can configure your CPAchecker "
        + "parameters which will be used in the CPAchecker execution.";
  }

  @Override
  public Icon getIcon() {
    return AllIcons.General.Balloon;
  }

  @NotNull
  @Override
  public String getId() {
    return "CPACHECKER_RUN_CONFIGURATION";
  }

  @Override
  public ConfigurationFactory[] getConfigurationFactories() {
    return new ConfigurationFactory[] {new CpacheckerConfigurationFactory(this)};
  }
}
