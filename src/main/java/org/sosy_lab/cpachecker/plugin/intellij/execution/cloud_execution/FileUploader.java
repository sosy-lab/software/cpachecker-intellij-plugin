package org.sosy_lab.cpachecker.plugin.intellij.execution.cloud_execution;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;

class FileUploader {
  private URL url;

  FileUploader(String requestURL) throws IOException {
    url = new URL(requestURL);
  }

  boolean performUpload(String path) throws IOException {
    HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
    try {
      httpConn.setUseCaches(false);
      httpConn.setDoOutput(true); // indicates POST method
      httpConn.setDoInput(true);
      httpConn.addRequestProperty("Content-Type", "application/octet-stream");
      httpConn.addRequestProperty("User-Agent", CloudConnectionConfigs.USER_AGENT_CONTENT);
      try (OutputStream outputStream = httpConn.getOutputStream()) {
        File file = new File(path);
        Files.copy(file.toPath(), outputStream);
      }
      int status = httpConn.getResponseCode();
      return status == CloudConnectionConfigs.SUCCESS_NO_CONENT_RESPONSE;
    } finally {
      httpConn.disconnect();
    }
  }
}
