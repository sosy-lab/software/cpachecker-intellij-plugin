package org.sosy_lab.cpachecker.plugin.intellij.execution.exceptions;

public class CPACheckerConfigurationException extends Exception {
  public CPACheckerConfigurationException(String msg) {
    super(msg);
  }
}
