package org.sosy_lab.cpachecker.plugin.intellij.execution.notification;

import com.intellij.notification.Notification;
import com.intellij.notification.NotificationDisplayType;
import com.intellij.notification.NotificationGroup;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;

public class NotificationCreator {
  public NotificationCreator() {}

  public void createNotification(
      String displayId, String content, NotificationType notificationType, Project project) {
    ApplicationManager.getApplication()
        .invokeLater(
            () -> {
              Notification notification =
                  new NotificationGroup(displayId, NotificationDisplayType.BALLOON, true)
                      .createNotification(content, notificationType);
              Notifications.Bus.notify(notification, project);
            });
  }
}
