package org.sosy_lab.cpachecker.plugin.intellij.execution.cloud_execution;

class CloudConnectionConfigs {
  static final String SPECIFICATION_TEXT = "specificationText";
  static final String PROPERTY_TEXT = "propertyText";
  static final String WITNESS_TEXT = "witnessText";
  static final String META_INFORMATION = "metaInformation";
  static final String CPU_MODEL = "cpuModel";
  static final String CONFIGURATION = "configuration";
  static final String SPECIFICATION = "specification";
  static final String TIME_LIMITATION = "timeLimitation";
  static final String OPTION = "option";
  static final String REVISION = "revision";
  static final String CORE_LIMITATION = "coreLimitation";
  static final String MEMORY_LIMITATION = "memoryLimitation";
  static final String USER_AGENT_CONTENT =
      "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko)"
          + " Chrome/23.0.1271.95 Safari/537.11";
  static final String SUBMIT_RUN_URL = "https://vcloud.sosy-lab.org/cpachecker/webclient/run/";
  static final String FILE_UPLOAD_URL = "https://vcloud.sosy-lab.org/cpachecker/webclient/files/";
  static final int SUCCESS_NO_CONENT_RESPONSE = 204;

  private CloudConnectionConfigs() {}
}
