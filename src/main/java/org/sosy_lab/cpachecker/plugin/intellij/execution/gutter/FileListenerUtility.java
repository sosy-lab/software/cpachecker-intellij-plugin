package org.sosy_lab.cpachecker.plugin.intellij.execution.gutter;

import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.markup.HighlighterLayer;
import com.intellij.openapi.editor.markup.HighlighterTargetArea;
import com.intellij.openapi.editor.markup.RangeHighlighter;
import com.intellij.openapi.editor.markup.TextAttributes;

class FileListenerUtility {
  private FileListenerUtility() {}

  static RangeHighlighter createRangeHighlighter(
      Document document, Editor editor, int fromLine, int toLine, TextAttributes attributes) {
    int lineStartOffset = document.getLineStartOffset(Math.max(0, fromLine));
    int lineEndOffset = document.getLineEndOffset(Math.max(0, toLine));

    return editor
        .getMarkupModel()
        .addRangeHighlighter(
            lineStartOffset,
            lineEndOffset,
            HighlighterLayer.ADDITIONAL_SYNTAX,
            attributes,
            HighlighterTargetArea.LINES_IN_RANGE);
  }
}
