package org.sosy_lab.cpachecker.plugin.intellij.execution.linux_cmd;

import com.intellij.execution.ExecutionException;
import com.intellij.execution.configurations.GeneralCommandLine;
import com.intellij.execution.process.ColoredProcessHandler;
import com.intellij.ide.plugins.PluginManager;
import com.intellij.notification.NotificationType;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import java.util.concurrent.TimeUnit;
import org.jetbrains.annotations.NotNull;
import org.sosy_lab.cpachecker.plugin.intellij.execution.ConfigurationUiConstants;
import org.sosy_lab.cpachecker.plugin.intellij.execution.CpacheckerRunConfiguration;
import org.sosy_lab.cpachecker.plugin.intellij.execution.console.ConsoleLogger;
import org.sosy_lab.cpachecker.plugin.intellij.execution.exceptions.CPACheckerConfigurationException;
import org.sosy_lab.cpachecker.plugin.intellij.execution.notification.NotificationCreator;

public class CmdLinuxExecution {
  private CpacheckerRunConfiguration cpacheckerRunConfiguration;
  private Project project;
  private String executionFunctionName;
  private String srcFilePath;
  private String entryFunctionParamString;
  private Process process;
  private volatile boolean processCreated;
  private ColoredProcessHandler handler;
  private GeneralCommandLine command;
  /** When plugin used in linux environment this class will be used for command line execution. */
  public CmdLinuxExecution(CpacheckerRunConfiguration cpacheckerRunConfiguration, Project project)
      throws CPACheckerConfigurationException {
    this.cpacheckerRunConfiguration = cpacheckerRunConfiguration;
    this.project = project;
    this.executionFunctionName = getEntryFunctionPropertyStringFromConfig();
    this.srcFilePath = getFilePathFromConfig();
    this.entryFunctionParamString = getEntryFunctionParamStringBasedOnConfig();
    processCreated = false;
  }

  public CmdLinuxExecution(
      CpacheckerRunConfiguration cpacheckerRunConfiguration,
      Project project,
      String functionName,
      String srcFilePath) {
    this.cpacheckerRunConfiguration = cpacheckerRunConfiguration;
    this.project = project;
    this.srcFilePath = srcFilePath;
    this.executionFunctionName = functionName;
    this.entryFunctionParamString = "-entryfunction";
    processCreated = false;
  }

  /** This method can be used for executing command lines. */
  public void process() throws CPACheckerConfigurationException {
    command = new GeneralCommandLine();
    command.setExePath(getExecutablePath());
    String setPropString = "-setprop";
    command.addParameters(setPropString, getLogLevelPropertyString());
    if (cpacheckerRunConfiguration.getConfigFilePathString() != null) {
      command.addParameters("-config", cpacheckerRunConfiguration.getConfigFilePathString());
    }
    if (cpacheckerRunConfiguration.getSpecFilePathString() != null) {
      command.addParameters("-spec", cpacheckerRunConfiguration.getSpecFilePathString());
    }
    if (cpacheckerRunConfiguration.getTimeLimitationSeconds() != null
        && Integer.parseInt(cpacheckerRunConfiguration.getTimeLimitationSeconds()) != 0) {
      command.addParameters("-timelimit", cpacheckerRunConfiguration.getTimeLimitationSeconds());
    }
    if (cpacheckerRunConfiguration.isWitnessValidationOn()) {
      if (cpacheckerRunConfiguration.getWitnessValidationFilePathString() != null) {
        command.addParameters(
            "-witness", cpacheckerRunConfiguration.getWitnessValidationFilePathString());
      }
    }
    command.addParameters(entryFunctionParamString, executionFunctionName, srcFilePath);
    ProgressManager.getInstance()
        .run(
            new Task.Backgroundable(
                CpacheckerRunConfiguration.getCurrentProject(), "CPAchecker is running locally") {
              public void run(@NotNull ProgressIndicator indicator) {
                indicator.setIndeterminate(false);
                NotificationCreator notificationCreator = new NotificationCreator();
                ApplicationManager.getApplication()
                    .invokeLater(
                        new Runnable() {
                          @Override
                          public void run() {
                            FileDocumentManager.getInstance()
                                .saveAllDocuments(); // whenever process method is called we should
                            // make sure
                            // all the local changes are saved.
                            runCommand();
                          }
                        });
                while (!processCreated) {
                  try {
                    Thread.sleep(1000);
                  } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                  }
                }
                if (!Thread.interrupted()) {
                  while (process != null) {
                    try {
                      TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                      e.printStackTrace();
                    }
                    if (indicator.isCanceled()) {
                      handler.setShouldKillProcessSoftly(true);
                      handler.destroyProcess();
                      indicator.cancel();
                      notificationCreator.createNotification(
                          ConfigurationUiConstants.UNABLE_TO_RUN_NOTIFICATION,
                          "Execution canceled",
                          NotificationType.ERROR,
                          CpacheckerRunConfiguration.getCurrentProject());
                      break;
                    }
                    if (!process.isAlive()) {
                      indicator.cancel();
                      break;
                    }
                  }
                }
              }
            });
  }

  private boolean isValidCPADirectory() {
    VirtualFile virtualFile =
        LocalFileSystem.getInstance()
            .findFileByPath(cpacheckerRunConfiguration.getCpaDir() + "/" + "scripts/cpa.sh");
    return virtualFile != null;
  }

  private String getExecutablePath() throws CPACheckerConfigurationException {
    if (!isValidCPADirectory()) {
      throw new CPACheckerConfigurationException(
          "No valid CPA directory specified in run configuration");
    }
    String cpa = cpacheckerRunConfiguration.getCpaDir() + "/" + "scripts/cpa.sh";
    return cpa;
  }

  @NotNull
  private String getFilePathFromConfig() throws CPACheckerConfigurationException {
    if (cpacheckerRunConfiguration.getSrcFileForExecution() == null
        || cpacheckerRunConfiguration.getSrcFileForExecution().equals("")) {
      throw new CPACheckerConfigurationException(
          "In configuration UI no files selected for CPAcchecker execution");
    } else {
      return project.getBasePath() + cpacheckerRunConfiguration.getSrcFileForExecution();
    }
  }

  private String getLogLevelPropertyString() {
    return "log.consoleLevel=" + cpacheckerRunConfiguration.getLogLevelString();
  }

  private String getEntryFunctionParamStringBasedOnConfig()
      throws CPACheckerConfigurationException {
    if (cpacheckerRunConfiguration.getFunctionForExecution() == null
        || cpacheckerRunConfiguration.getFunctionForExecution().equals("")) {
      throw new CPACheckerConfigurationException(
          "In configuration UI no function selected for CPAcchecker execution");
    }
    return "-entryfunction";
  }

  private String getEntryFunctionPropertyStringFromConfig()
      throws CPACheckerConfigurationException {
    if (cpacheckerRunConfiguration.getFunctionForExecution() == null
        || cpacheckerRunConfiguration.getFunctionForExecution().equals("")) {
      throw new CPACheckerConfigurationException(
          "In configuration UI no function selected for CPAcchecker execution");
    }
    return cpacheckerRunConfiguration.getFunctionForExecution();
  }

  private void runCommand() {
    try {
      process = command.createProcess();
      handler = new ColoredProcessHandler(process, command.getCommandLineString());
      processCreated = true;
      ConsoleLogger consoleLogger = new ConsoleLogger();
      VirtualFile currentSrcVirtualFile = LocalFileSystem.getInstance().findFileByPath(srcFilePath);
      consoleLogger.displayCommandLineLog("CPAchecker", project, handler, currentSrcVirtualFile);
    } catch (ExecutionException err) {
      err.printStackTrace();
      PluginManager.getLogger().error(err);
    } finally {
      processCreated = true;
    }
  }
}
