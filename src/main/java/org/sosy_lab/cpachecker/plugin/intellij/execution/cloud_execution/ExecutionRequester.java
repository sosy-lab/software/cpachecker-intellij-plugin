package org.sosy_lab.cpachecker.plugin.intellij.execution.cloud_execution;

import com.google.common.collect.Multimap;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import javax.annotation.Nullable;

class ExecutionRequester {
  private URL url;
  private int responseCode;

  ExecutionRequester(String urlString) throws MalformedURLException {
    this.url = new URL(urlString);
  }

  @Nullable
  String triggerExecution(Multimap<String, String> arguments) throws IOException {
    URLConnection con = url.openConnection();
    HttpURLConnection http = (HttpURLConnection) con;
    http.setRequestMethod("POST");
    http.setDoOutput(true);
    http.setUseCaches(false);
    http.setDoInput(true);
    http.addRequestProperty("Accept", "text/plain");
    http.addRequestProperty("User-Agent", CloudConnectionConfigs.USER_AGENT_CONTENT);
    String formByteData = prepareFormDataString(arguments);
    int length = formByteData.length();
    http.setFixedLengthStreamingMode(length);
    http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    try {
      http.connect();
      try (OutputStreamWriter os = new OutputStreamWriter(http.getOutputStream())) {
        os.write(formByteData);
      }

      // checks server's status code first
      int status = http.getResponseCode();
      responseCode = status;
      if (status == HttpURLConnection.HTTP_OK) {
        String response;
        try (BufferedReader reader =
            new BufferedReader(new InputStreamReader(http.getInputStream()))) {
          response = reader.lines().collect(Collectors.joining("\n"));
        }
        return response;
      } else {
        return null;
      }
    } finally {
      http.disconnect();
    }
  }

  public int getResponseCode() {
    return responseCode;
  }

  private String prepareFormDataString(Multimap<String, String> arguments)
      throws UnsupportedEncodingException {
    StringJoiner sj = new StringJoiner("&");
    for (Map.Entry entry : arguments.entries()) {
      sj.add(
          URLEncoder.encode(entry.getKey().toString(), "UTF-8")
              + "="
              + URLEncoder.encode(entry.getValue().toString(), "UTF-8"));
    }
    return sj.toString();
  }
}
