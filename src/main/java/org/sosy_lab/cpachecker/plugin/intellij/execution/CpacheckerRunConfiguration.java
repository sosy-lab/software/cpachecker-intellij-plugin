package org.sosy_lab.cpachecker.plugin.intellij.execution;

import com.intellij.execution.Executor;
import com.intellij.execution.configurations.ConfigurationFactory;
import com.intellij.execution.configurations.RunConfiguration;
import com.intellij.execution.configurations.RunConfigurationBase;
import com.intellij.execution.configurations.RunProfileState;
import com.intellij.execution.runners.ExecutionEnvironment;
import com.intellij.ide.plugins.PluginManager;
import com.intellij.notification.NotificationType;
import com.intellij.openapi.options.SettingsEditor;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.sosy_lab.cpachecker.plugin.intellij.execution.cloud_execution.CloudExecutor;
import org.sosy_lab.cpachecker.plugin.intellij.execution.exceptions.CPACheckerConfigurationException;
import org.sosy_lab.cpachecker.plugin.intellij.execution.linux_cmd.CmdLinuxExecution;
import org.sosy_lab.cpachecker.plugin.intellij.execution.notification.NotificationCreator;

public class CpacheckerRunConfiguration extends RunConfigurationBase {
  private static CpacheckerRunConfiguration cpacheckerRunConfiguration;
  private static Project currentProject;

  public CpacheckerRunConfiguration(
      String name, Project project, ConfigurationFactory configurationFactory) {
    super(project, configurationFactory, name);
    currentProject = project;
    setCpaCheckerRunconfigurationInstance(this);
  }

  public static void setCpaCheckerRunconfigurationInstance(
      CpacheckerRunConfiguration cpaCheckerRunconfigurationInstance) {
    cpacheckerRunConfiguration = cpaCheckerRunconfigurationInstance;
  }

  @Contract(pure = true)
  public static CpacheckerRunConfiguration getCpacheckerRunConfiguration() {
    return cpacheckerRunConfiguration;
  }

  public static Project getCurrentProject() {
    return currentProject;
  }

  public String getRevisionString() {
    return ((ConfigurationInfo) getOptions()).getRevisionString();
  }

  public void setRevisionString(String revisionString) {
    ((ConfigurationInfo) getOptions()).setRevisionString(revisionString);
  }

  public String getConfigFilePathString() {
    return ((ConfigurationInfo) getOptions()).getConfigFilePathString();
  }

  public void setConfigFilePathString(String configString) {
    ((ConfigurationInfo) getOptions()).setConfigFilePathString(configString);
  }

  public String getSpecComboString() {
    return ((ConfigurationInfo) getOptions()).getSpecComboString();
  }

  public void setSpecComboString(String specComboString) {
    ((ConfigurationInfo) getOptions()).setSpecComboString(specComboString);
  }

  public String getSpecAreaText() {
    return ((ConfigurationInfo) getOptions()).getSpecAreaText();
  }

  public void setSpecAreaText(String specAreaText) {
    ((ConfigurationInfo) getOptions()).setSpecAreaText(specAreaText);
  }

  public boolean isWitnessValidationOn() {
    return ((ConfigurationInfo) getOptions()).isWitnessValidationOn();
  }

  public void setWitnessValidationOn(boolean on) {
    ((ConfigurationInfo) getOptions()).setWitnessValidationOn(on);
  }

  public String getLogLevelString() {
    return ((ConfigurationInfo) getOptions()).getLogLevelString();
  }

  public void setLogLevelString(String logLevelString) {
    ((ConfigurationInfo) getOptions()).setLogLevelString(logLevelString);
  }

  public String getMachineModelString() {
    return ((ConfigurationInfo) getOptions()).getMachineModelString();
  }

  public void setMachineModelString(String machineModelString) {
    ((ConfigurationInfo) getOptions()).setMachineModelString(machineModelString);
  }

  public String getMemoryLimitationString() {
    return ((ConfigurationInfo) getOptions()).getMemoryLimitationString();
  }

  public void setMemoryLimitationString(String memoryLimitationString) {
    ((ConfigurationInfo) getOptions()).setMemoryLimitationString(memoryLimitationString);
  }

  public String getCoreLimitationString() {
    return ((ConfigurationInfo) getOptions()).getCoreLimitationString();
  }

  public void setCoreLimitationString(String coreLimitationString) {
    ((ConfigurationInfo) getOptions()).setCoreLimitationString(coreLimitationString);
  }

  public boolean isCloud() {
    return ((ConfigurationInfo) getOptions()).isCloud();
  }

  public void setCloud(boolean cloud) {
    ((ConfigurationInfo) getOptions()).setCloud(cloud);
  }

  public boolean isGraphicOutputOn() {
    return ((ConfigurationInfo) getOptions()).isGraphicOutputOn();
  }

  public void setGraphicOutputOn(boolean on) {
    ((ConfigurationInfo) getOptions()).setGraphicOutputOn(on);
  }

  @Override
  public SettingsEditor<? extends RunConfiguration> getConfigurationEditor() {
    return new CpaRunConfigurationSettingsEditor();
  }

  @Nullable
  @Override
  public RunProfileState getState(
      @NotNull Executor executor, @NotNull final ExecutionEnvironment env) {
    if (isCloud()) {
      new Thread(
              () -> {
                CloudExecutor cloudExecutor = new CloudExecutor(this);
                cloudExecutor.execute();
              })
          .start();
      return null;
    }
    System.out.println(System.getProperty("os.name"));
    if (System.getProperty("os.name").toLowerCase().equals("linux")) {
      try {
        CmdLinuxExecution cmdLinuxExecution = new CmdLinuxExecution(this, env.getProject());
        NotificationCreator notificationCreator = new NotificationCreator();
        try {
          cmdLinuxExecution.process();
        } catch (CPACheckerConfigurationException error) {
          error.printStackTrace();
          notificationCreator.createNotification(
              ConfigurationUiConstants.UNABLE_TO_RUN_NOTIFICATION,
              "Execution exception : " + error.getMessage(),
              NotificationType.ERROR,
              CpacheckerRunConfiguration.getCurrentProject());
        }
      } catch (CPACheckerConfigurationException err) {
        err.printStackTrace();
        PluginManager.getLogger().error(err);
      }
    }
    return null;
  }

  public String getTimeLimitationSeconds() {
    return ((ConfigurationInfo) getOptions()).getTimeLimitationSeconds();
  }

  public void setTimeLimitationSeconds(String timeLimitationFieldText) {
    ((ConfigurationInfo) getOptions()).setTimeLimitationSeconds(timeLimitationFieldText);
  }

  public String getCpaDir() {
    return ((ConfigurationInfo) getOptions()).getCpaDir();
  }

  public void setCpaDir(String cpaDir) {
    ((ConfigurationInfo) getOptions()).setCpaDir(cpaDir);
  }

  public String getSpecFilePathString() {
    return ((ConfigurationInfo) getOptions()).getSpecFilePathString();
  }

  public void setSpecFilePathString(String specFilePathString) {
    ((ConfigurationInfo) getOptions()).setSpecFilePathString(specFilePathString);
  }

  public String getWitnessValidationFilePathString() {
    return ((ConfigurationInfo) getOptions()).getWitnessValidationFilePathString();
  }

  public void setWitnessValidationFilePathString(String witnessValidationFilePathString) {
    ((ConfigurationInfo) getOptions())
        .setWitnessValidationFilePathString(witnessValidationFilePathString);
  }

  public String getSrcFileForExecution() {
    return ((ConfigurationInfo) getOptions()).getSrcFileForExecution();
  }

  public void setSrcFileForExecution(String srcFilePath) {
    ((ConfigurationInfo) getOptions()).setSrcFileForExecution(srcFilePath);
  }

  public String getFunctionForExecution() {
    return ((ConfigurationInfo) getOptions()).getFunctionForExecution();
  }

  public void setFunctionForExecution(String functionName) {
    ((ConfigurationInfo) getOptions()).setFunctionForExecution(functionName);
  }
}
