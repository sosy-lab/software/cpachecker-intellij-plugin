package org.sosy_lab.cpachecker.plugin.intellij.execution.cloud_execution;

import com.google.common.collect.Multimap;
import com.google.common.hash.Funnels;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import com.intellij.notification.NotificationType;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.jetbrains.annotations.NotNull;
import org.sosy_lab.cpachecker.plugin.intellij.execution.ConfigurationUiConstants;
import org.sosy_lab.cpachecker.plugin.intellij.execution.CpacheckerRunConfiguration;
import org.sosy_lab.cpachecker.plugin.intellij.execution.console.ConsoleLogger;
import org.sosy_lab.cpachecker.plugin.intellij.execution.exceptions.ExecutionException;
import org.sosy_lab.cpachecker.plugin.intellij.execution.notification.NotificationCreator;

/** Contains methods to connect with CPAchecker server. */
public class CloudExecutor {
  private final CloudExecutor cloudExecutor;
  private CpacheckerRunConfiguration cpacheckerRunConfiguration;
  private String path;
  private String extractedPath;
  private volatile ProgressIndicator progressIndicator;
  private Thread cloudExecutorWorkerThread;
  private boolean isExecutable;
  private Semaphore indicatorCreated;

  /**
   * Creates a new CloudExecutor.
   *
   * @param cpacheckerRunConfiguration CpacheckerRunConfiguration object will be used to get
   *     configuration values.
   */
  public CloudExecutor(CpacheckerRunConfiguration cpacheckerRunConfiguration) {
    this.cpacheckerRunConfiguration = cpacheckerRunConfiguration;
    this.cloudExecutor = this;
    this.isExecutable = true;
    indicatorCreated = new Semaphore(0);
    createTempDir();
  }

  /**
   * Extracts a zipped file.
   *
   * @param zipFilePath zip file path.
   * @param destDir path for the destination folder.
   */
  private static void unzip(String zipFilePath, String destDir) throws IOException {
    File dir = new File(destDir);
    // create output directory if it doesn't exist
    if (!dir.exists()) {
      dir.mkdirs();
    }
    try (FileInputStream fis = new FileInputStream(zipFilePath);
        ZipInputStream zis = new ZipInputStream(fis)) {
      ZipEntry ze = zis.getNextEntry();
      try {
        while (ze != null) {
          String fileName = ze.getName();
          File newFile = new File(destDir + File.separator + fileName);
          if (newFile.getPath().contains("output/")) {
            zis.closeEntry();
            ze = zis.getNextEntry();
            continue;
          }
          new File(newFile.getParent()).mkdirs();
          Files.copy(zis, newFile.toPath());
          // close this ZipEntry
          zis.closeEntry();
          ze = zis.getNextEntry();
        }
      } finally {
        // close last ZipEntry
        zis.closeEntry();
      }
    }
  }

  /** creates temporary folder for downloading result as zip and to extract that zip file. */
  private void createTempDir() {
    try {
      Path tempPath = Files.createTempDirectory("CPAchecker");
      this.path = tempPath.toAbsolutePath() + "/result";
      this.extractedPath = path + "Extracted";
    } catch (IOException error) {
      error.printStackTrace();
    }
  }

  private ProgressIndicator getProgressIndicator() {
    return progressIndicator;
  }

  private void setProgressIndicator(ProgressIndicator progressIndicator) {
    this.progressIndicator = progressIndicator;
  }

  /**
   * Triggers bunch of cloud execution related requests. If this method is called more than once on
   * a CloudExecutor object, it will throw IllegalStateException.
   *
   * @throws IllegalStateException if method is called more than once.
   * @throws InterruptedException if thread is interrupted by its parent thread when user cancel the
   *     execution.
   */
  public synchronized void execute() {
    if (isExecutable) {
      isExecutable = false;
    } else {
      throw new IllegalStateException(
          "CloudExecutor#execute method only one time callable per CloudExecutor object");
    }
    showProgressAndExecuteOnCloud();
    try {
      indicatorCreated.acquire();
      while (progressIndicator.isRunning()) {
        try {
          TimeUnit.MILLISECONDS.sleep(500);
          if (progressIndicator.isCanceled()) {
            cloudExecutorWorkerThread.interrupt();
            break;
          }
        } catch (InterruptedException error) {
          error.printStackTrace();
        }
      }
    } catch (InterruptedException error) {
      error.printStackTrace();
    }
  }

  /**
   * Starts the execution in the cloud.
   *
   * @throws IOException sub methods may throw while creating files or while doing other IO tasks.
   * @throws InterruptedException if thread is interrupted by its parent thread when user cancel the
   *     execution.
   * @throws ExecutionException if server returned a non OK response.
   */
  private void startCloudProcessing() throws IOException, InterruptedException, ExecutionException {
    ExecutionRequester executionRequester =
        new ExecutionRequester(CloudConnectionConfigs.SUBMIT_RUN_URL);
    Multimap<String, String> arguments = prepareArguments();
    setFractionAndContent(0.0f, "Trying to connect to the CPAchecker server");
    String response = executionRequester.triggerExecution(arguments);
    boolean loaded = true;
    if (response == null) {
      setFractionAndContent(0.2f, "Uploading programming file");
      FileUploader fileUploader = new FileUploader(CloudConnectionConfigs.FILE_UPLOAD_URL);
      loaded =
          fileUploader.performUpload(
              cpacheckerRunConfiguration.getProject().getBasePath()
                  + cpacheckerRunConfiguration.getSrcFileForExecution());
      response = executionRequester.triggerExecution(arguments);
    }
    if (loaded && (response != null)) {
      setFractionAndContent(0.4f, "CPAchecker is running on the cloud");
      executionTriggered(response);
    } else {
      throw new ExecutionException(
          "Server returned " + executionRequester.getResponseCode() + " response code.");
    }
  }

  /**
   * Sets the content and fraction of the progress indicator.
   *
   * @param fraction a float value which will be the progress fraction of the indicator.
   * @param content a string value which will be displayed in the progress bar.
   */
  private void setFractionAndContent(float fraction, String content) {
    getProgressIndicator().setText(content);
    getProgressIndicator().setFraction(fraction);
  }

  private void setCloudExecutorWorkerThread(Thread thread) {
    cloudExecutorWorkerThread = thread;
  }

  /**
   * creates a progress bar instance and a thread which will be responsible for updating progress
   * bar content, fraction and to act as a connector with the server.
   */
  private void showProgressAndExecuteOnCloud() {
    ProgressManager.getInstance()
        .run(
            new Task.Backgroundable(
                CpacheckerRunConfiguration.getCurrentProject(), "CPAchecker is running on cloud") {
              public void run(@NotNull ProgressIndicator indicator) {
                setCloudExecutorWorkerThread(Thread.currentThread());
                setProgressIndicator(indicator);
                indicatorCreated.release();
                indicator.setIndeterminate(false);
                NotificationCreator notificationCreator = new NotificationCreator();
                String errorToNotify = "";
                try {
                  startCloudProcessing();
                } catch (ExecutionException e) {
                  errorToNotify = "Execution failed due to : " + e.getMessage();
                } catch (InterruptedException e) {
                  errorToNotify = "Cloud execution canceled";
                } catch (IOException e) {
                  e.printStackTrace();
                } finally {
                  if (!errorToNotify.equals("")) {
                    notificationCreator.createNotification(
                        ConfigurationUiConstants.UNABLE_TO_RUN_NOTIFICATION,
                        errorToNotify,
                        NotificationType.ERROR,
                        CpacheckerRunConfiguration.getCurrentProject());
                    indicator.cancel();
                  }
                }
              }

              @Override
              public void onSuccess() {
                super.onSuccess();
                try {
                  displayLogs();
                } catch (FileNotFoundException error) {
                  NotificationCreator notificationCreator = new NotificationCreator();
                  notificationCreator.createNotification(
                      ConfigurationUiConstants.UNABLE_TO_RUN_NOTIFICATION,
                      "Could not display logs",
                      NotificationType.ERROR,
                      CpacheckerRunConfiguration.getCurrentProject());
                  error.printStackTrace();
                  delete(new File(path).getParentFile());
                }
              }

              @Override
              public void onCancel() {
                super.onCancel();
                delete(new File(path).getParentFile());
              }
            });
  }

  /**
   * Must be called after execution request with the returned id.
   *
   * @param id is returned from the CPAchecker server as response to the execution request.
   * @throws IOException sub methods may throw while creating files or while doing other IO tasks.
   * @throws InterruptedException if thread is interrupted by its parent thread when user cancel the
   *     execution.
   */
  private void executionTriggered(String id) throws IOException, InterruptedException {
    waitForExecutionCompletion(id);
    setFractionAndContent(0.6f, "CPAchecker execution finished in cloud");
    GetRequestMaker getRequestMaker = new GetRequestMaker();
    getRequestMaker.downloadFile(CloudConnectionConfigs.SUBMIT_RUN_URL + id + "/result", path);
    setFractionAndContent(0.8f, "Results downloaded");
    unzip(path, extractedPath);
    setFractionAndContent(1.0f, "Finished");
  }

  /**
   * displays logs returned by the server. Will be executed by parent thread and it should wait
   * while cloud connector thread working. This method must be run in the AWT thread.
   *
   * @throws IOException sub methods may throw while creating files or while doing other IO tasks.
   */
  private void displayLogs() throws FileNotFoundException {
    List<String> logs = getLogdata(extractedPath);
    ConsoleLogger consoleLogger = new ConsoleLogger();
    VirtualFile currentSrcVirtualFile =
        LocalFileSystem.getInstance()
            .findFileByPath(
                cpacheckerRunConfiguration.getProject().getBasePath()
                    + cpacheckerRunConfiguration.getSrcFileForExecution());
    consoleLogger.displayCloudExecutionLog(
        "CPAchecker", cpacheckerRunConfiguration.getProject(), logs, currentSrcVirtualFile);
    delete(new File(path).getParentFile());
  }

  /**
   * prepares option value pairs as MultiMap.
   *
   * @return argument MultiMap which will be used by execution request.
   * @throws IOException sub methods may throw while creating files or while doing other IO tasks.
   */
  private Multimap<String, String> prepareArguments() throws IOException {
    File file =
        new File(
            cpacheckerRunConfiguration.getProject().getBasePath()
                + cpacheckerRunConfiguration.getSrcFileForExecution());
    Hasher hasher = Hashing.sha256().newHasher();
    Files.copy(file.toPath(), Funnels.asOutputStream(hasher));
    HashCode hashCode = hasher.hash();

    OptionValueContainer arguments = new OptionValueContainer();
    arguments.put("programTextHash", hashCode.toString());
    if (cpacheckerRunConfiguration.getTimeLimitationSeconds() != null) { // 05min 00s
      int totalSeconds = Integer.parseInt(cpacheckerRunConfiguration.getTimeLimitationSeconds());
      if (totalSeconds > 0) {
        int seconds = totalSeconds % 60;
        int minutes = totalSeconds / 60;
        String time = minutes + "min " + seconds + "s";
        arguments.put(CloudConnectionConfigs.TIME_LIMITATION, time);
      }
    }
    if (cpacheckerRunConfiguration.getSpecAreaText() != null) {
      arguments.put(
          CloudConnectionConfigs.PROPERTY_TEXT, cpacheckerRunConfiguration.getSpecAreaText());
    }
    arguments.put(
        CloudConnectionConfigs.OPTION,
        "log.level=" + cpacheckerRunConfiguration.getLogLevelString());
    arguments.put(CloudConnectionConfigs.REVISION, cpacheckerRunConfiguration.getRevisionString());
    if (cpacheckerRunConfiguration.getConfigFilePathString() != null) {
      arguments.put(
          CloudConnectionConfigs.CONFIGURATION,
          getConfigNameFromPath(cpacheckerRunConfiguration.getConfigFilePathString()));
    }
    arguments.put(
        CloudConnectionConfigs.OPTION,
        "analysis.entryFunction=" + cpacheckerRunConfiguration.getFunctionForExecution());
    if (cpacheckerRunConfiguration.getCoreLimitationString() != null) {
      arguments.put(
          CloudConnectionConfigs.CORE_LIMITATION,
          cpacheckerRunConfiguration.getCoreLimitationString());
    }
    if (cpacheckerRunConfiguration.getMemoryLimitationString() != null) {
      arguments.put(
          CloudConnectionConfigs.MEMORY_LIMITATION,
          cpacheckerRunConfiguration.getMemoryLimitationString());
    }
    arguments.put(
        CloudConnectionConfigs.OPTION,
        "analysis.machineModel=" + cpacheckerRunConfiguration.getMachineModelString());
    if (cpacheckerRunConfiguration.isWitnessValidationOn()) {
      if (cpacheckerRunConfiguration.getWitnessValidationFilePathString() != null) {
        String witnessText;
        try (BufferedReader br =
            new BufferedReader(
                new FileReader(cpacheckerRunConfiguration.getWitnessValidationFilePathString()))) {
          witnessText = br.lines().collect(Collectors.joining("\n"));
        }
        if (witnessText != null) {
          arguments.put(CloudConnectionConfigs.WITNESS_TEXT, witnessText);
        }
      }
    }
    return arguments;
  }

  private String getConfigNameFromPath(String pathToExtract) {
    return pathToExtract.substring(
        pathToExtract.lastIndexOf("/") + 1, pathToExtract.lastIndexOf("."));
  }

  /**
   * waits while execution return a state.
   *
   * @param id is returned by server for the execution request.
   * @throws IOException sub methods may throw while creating files or while doing other IO tasks.
   * @throws InterruptedException if thread is interrupted by its parent thread when user cancel the
   *     execution.
   */
  private void waitForExecutionCompletion(String id) throws IOException, InterruptedException {
    String url = CloudConnectionConfigs.SUBMIT_RUN_URL + id + "/state";
    GetRequestMaker getRequestMaker = new GetRequestMaker();
    String state;
    do {
      TimeUnit.SECONDS.sleep(1);
      state = getRequestMaker.makeGetRequest(url).toLowerCase();
    } while (!returnedAState(state));
  }

  private boolean returnedAState(String state) {
    String stateFinished = "finished";
    String stateUnknown = "unknown";
    String stateError = "error";
    String stateProcessing = "processing";
    if (state.equals(stateFinished) || state.equals(stateError) || state.equals(stateUnknown)) {
      return true;
    } else if (state.equals(stateProcessing)) {
      return false;
    }
    throw new IllegalStateException("Server returned a unexpected state");
  }

  private List<String> getLogdata(String outputLogPath) throws FileNotFoundException {
    String logPath = outputLogPath + "/output.log";
    File file = new File(logPath);
    BufferedReader br = new BufferedReader(new FileReader(file));
    List<Object> resultList = new ArrayList<>(Arrays.asList(br.lines().toArray()));
    return resultList.stream()
        .map(object -> Objects.toString(object, null))
        .collect(Collectors.toList());
  }

  private void delete(File f) {
    if (f.isDirectory()) {
      for (File c : Objects.requireNonNull(f.listFiles())) {
        delete(c);
      }
    }
    if (!f.delete()) {
      NotificationCreator notificationCreator = new NotificationCreator();
      notificationCreator.createNotification(
          ConfigurationUiConstants.UNABLE_TO_RUN_NOTIFICATION,
          "Could not delete temporary folder.",
          NotificationType.ERROR,
          CpacheckerRunConfiguration.getCurrentProject());
    }
  }
}
