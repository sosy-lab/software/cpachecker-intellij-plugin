package org.sosy_lab.cpachecker.plugin.intellij.execution.cloud_execution;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ForwardingMultimap;
import com.google.common.collect.Multimap;

/**
 * Contains a MultiMap which will be used to store option value pair for making execution request to
 * CPAchecker server.
 */
public class OptionValueContainer extends ForwardingMultimap<String, String> {
  private final Multimap<String, String> delegate = ArrayListMultimap.create();

  @Override
  protected Multimap<String, String> delegate() {
    return delegate;
  }
}
