package org.sosy_lab.cpachecker.plugin.intellij.execution.console;

import com.intellij.execution.filters.TextConsoleBuilder;
import com.intellij.execution.filters.TextConsoleBuilderFactory;
import com.intellij.execution.process.ColoredProcessHandler;
import com.intellij.execution.ui.ConsoleView;
import com.intellij.execution.ui.ConsoleViewContentType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowAnchor;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import java.util.List;

/**
 * This class will be used for creating console window for display output from CPAchecker execution.
 */
public class ConsoleLogger {

  /**
   * Display ToolWindow containing output from CPAchecker execution.
   *
   * @param id of tool window to be registered.
   * @param project which is the current working project on IDE.
   * @param coloredProcessHandler A process handler which support ANSI coloring.
   * @param virtualFile which is the file used by CPAchecker for execution.
   */
  public void displayCommandLineLog(
      String id,
      Project project,
      ColoredProcessHandler coloredProcessHandler,
      VirtualFile virtualFile) {

    ConsoleView view = createView(id, project, virtualFile);
    coloredProcessHandler.startNotify();
    view.attachToProcess(coloredProcessHandler);
  }

  public void displayCloudExecutionLog(
      String id, Project project, List<String> logLines, VirtualFile virtualFile) {
    ConsoleView view = createView(id, project, virtualFile);
    for (String line : logLines) {
      view.print(line, ConsoleViewContentType.NORMAL_OUTPUT);
      view.print("\r\n", ConsoleViewContentType.NORMAL_OUTPUT);
    }
  }

  private ConsoleView createView(String id, Project project, VirtualFile virtualFile) {
    ToolWindowManager manager = ToolWindowManager.getInstance(project);
    TextConsoleBuilderFactory factory = TextConsoleBuilderFactory.getInstance();
    TextConsoleBuilder builder = factory.createBuilder(project);
    ConsoleView view = builder.getConsole();

    view.addMessageFilter(new HyperLinkMaker(project, virtualFile));
    ToolWindow window = manager.getToolWindow(id);
    if (window == null) {
      window = manager.registerToolWindow(id, true, ToolWindowAnchor.BOTTOM);
    }
    ContentFactory cf = window.getContentManager().getFactory();
    window.setToHideOnEmptyContent(false);
    Content[] contents = window.getContentManager().getContents();
    for (Content content : contents) {
      if (!content.isPinned()) {
        window.getContentManager().removeContent(content, true);
      }
    }
    Content content =
        cf.createContent(
            view.getComponent(),
            "Run" + (window.getContentManager().getContents().length + 1),
            true);
    window.getContentManager().addContent(content);
    window.getContentManager().setSelectedContent(content);
    window.setToHideOnEmptyContent(true);
    window.activate(null);
    return view;
  }
}
