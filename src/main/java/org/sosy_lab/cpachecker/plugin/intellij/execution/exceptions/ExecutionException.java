package org.sosy_lab.cpachecker.plugin.intellij.execution.exceptions;

public class ExecutionException extends Exception {
  public ExecutionException(String msg) {
    super(msg);
  }
}
