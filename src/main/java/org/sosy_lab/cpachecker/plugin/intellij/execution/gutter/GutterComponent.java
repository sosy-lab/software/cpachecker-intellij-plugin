package org.sosy_lab.cpachecker.plugin.intellij.execution.gutter;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.ProjectComponent;
import com.intellij.openapi.fileEditor.FileEditorManagerListener;
import com.intellij.util.messages.MessageBus;
import com.intellij.util.messages.MessageBusConnection;

public class GutterComponent implements ProjectComponent {
  private MessageBusConnection connection;

  @Override
  public void initComponent() {
    MessageBus bus = ApplicationManager.getApplication().getMessageBus();
    connection = bus.connect();
    connection.subscribe(
        FileEditorManagerListener.FILE_EDITOR_MANAGER, new CpaFileEditorManagerListener());
  }
}
