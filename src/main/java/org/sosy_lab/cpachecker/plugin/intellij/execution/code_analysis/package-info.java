/*
 *  CPAchecker is a tool for configurable software verification.
 *  This file is part of CPAchecker Clion plugin.
 *
 *
 *
 *  CPAchecker web page:
 *    http://cpachecker.sosy-lab.org
 */
/**
 * This package has implementation related to code analysis like search for functions, variables,
 * etc .
 */
package org.sosy_lab.cpachecker.plugin.intellij.execution.code_analysis;
