package org.sosy_lab.cpachecker.plugin.intellij.execution.console;

import com.intellij.execution.filters.Filter;
import com.intellij.execution.filters.HyperlinkInfo;
import com.intellij.execution.filters.OpenFileHyperlinkInfo;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jetbrains.annotations.Nullable;

public class HyperLinkMaker implements Filter {
  /**
   * According to the below regular expression a hyper link filter will be applied to the console
   * logs. If a match is found in the output, a navigation link to the specific file will be
   * attached to the (line [0-9]*) string.
   */
  private static final String REGEXP = "FALSE. Property violation \\(.*(line ([0-9]+))";

  private static int startOfLineIndex = 0;
  private static String lineNumberPrefixString = "line ";
  private Project project;
  private Pattern pattern;
  private VirtualFile myFile;

  public HyperLinkMaker(Project project, VirtualFile file) {
    this.project = project;
    pattern = Pattern.compile(REGEXP);
    myFile = file;
  }

  @Nullable
  @Override
  public Result applyFilter(String line, int entireLength) {
    Matcher matcher = pattern.matcher(line);
    if (!matcher.find()) {
      return null;
    }
    int lineNumber = Integer.parseInt(matcher.group(2));
    final int highlightStartOffset = entireLength - line.length() + matcher.start(1);
    final int highlightEndOffset = highlightStartOffset + matcher.end(1) - matcher.start(1);
    HyperlinkInfo hyperlinkInfo = createOpenFileHyperlink(lineNumber, startOfLineIndex);
    return new Result(highlightStartOffset, highlightEndOffset, hyperlinkInfo);
  }

  @Nullable
  protected HyperlinkInfo createOpenFileHyperlink(final int line, final int column) {
    return myFile != null
        ? new OpenFileHyperlinkInfo(
            project, myFile, line - 1,
            column) // currently when navigate to line number (line_n) it goes to line number
        // (line_n + 1)
        : null;
  }
}
