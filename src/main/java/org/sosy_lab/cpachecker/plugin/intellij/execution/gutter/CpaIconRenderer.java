package org.sosy_lab.cpachecker.plugin.intellij.execution.gutter;

import com.intellij.ide.plugins.PluginManager;
import com.intellij.notification.NotificationType;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.markup.GutterIconRenderer;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.IconLoader;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import javax.swing.Icon;
import org.jetbrains.annotations.Nullable;
import org.sosy_lab.cpachecker.plugin.intellij.execution.ConfigurationUiConstants;
import org.sosy_lab.cpachecker.plugin.intellij.execution.CpacheckerRunConfiguration;
import org.sosy_lab.cpachecker.plugin.intellij.execution.code_analysis.ObjectiveCVisitor;
import org.sosy_lab.cpachecker.plugin.intellij.execution.exceptions.CPACheckerConfigurationException;
import org.sosy_lab.cpachecker.plugin.intellij.execution.linux_cmd.CmdLinuxExecution;
import org.sosy_lab.cpachecker.plugin.intellij.execution.notification.NotificationCreator;

public class CpaIconRenderer extends GutterIconRenderer {

  private Project project;
  private Icon icon = IconLoader.getIcon("/icons/cpachecker_tick16.png");
  private int line;
  private Document document;

  public CpaIconRenderer(Project project, Document document) {
    this.project = project;
    this.document = document;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (!(obj instanceof CpaIconRenderer)) {
      return false;
    }

    CpaIconRenderer cpaIconRenderer = (CpaIconRenderer) obj;
    return cpaIconRenderer.icon.equals(icon) && cpaIconRenderer.line == line;
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + icon.hashCode();
    result = 31 * result + line;
    return result;
  }

  @Override
  public Icon getIcon() {
    return icon;
  }

  @Nullable
  @Override
  public AnAction getClickAction() {
    return new AnAction() {
      @Override
      public void actionPerformed(AnActionEvent err) {
        try {
          process();
        } catch (CPACheckerConfigurationException error) {
          error.printStackTrace();
          PluginManager.getLogger().error(error);
          NotificationCreator notificationCreator = new NotificationCreator();
          notificationCreator.createNotification(
              ConfigurationUiConstants.UNABLE_TO_RUN_NOTIFICATION,
              "Execution exception : " + error.getMessage(),
              NotificationType.ERROR,
              CpacheckerRunConfiguration.getCurrentProject());
        }
      }
    };
  }

  @Override
  public String getTooltipText() {
    return "Run CPAchecker on this function";
  }

  public void setLine(int val) {
    line = val;
  }

  /** using this process method we can run our command lines. */
  public void process() throws CPACheckerConfigurationException {
    VirtualFile currentFile = FileDocumentManager.getInstance().getFile(document);
    String srcFilePath = currentFile.getPath();

    PsiFile currentPsiFile = PsiManager.getInstance(project).findFile(currentFile);
    ObjectiveCVisitor objectiveCVisitor = new ObjectiveCVisitor();
    currentPsiFile.accept(objectiveCVisitor);
    int startOffset = document.getLineStartOffset(line);
    String functionName = "";
    for (String key : objectiveCVisitor.getFunctionAndStartIndexes().keySet()) {
      if (objectiveCVisitor.getFunctionAndStartIndexes().get(key) == startOffset) {
        functionName = key;
      }
    }
    CmdLinuxExecution cmdLinuxExecution =
        new CmdLinuxExecution(
            CpacheckerRunConfiguration.getCpacheckerRunConfiguration(),
            project,
            functionName,
            srcFilePath);
    cmdLinuxExecution.process();
  }
}
