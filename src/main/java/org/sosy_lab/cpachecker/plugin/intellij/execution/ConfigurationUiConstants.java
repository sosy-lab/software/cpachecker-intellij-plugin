package org.sosy_lab.cpachecker.plugin.intellij.execution;

public class ConfigurationUiConstants {
  public static final String UNABLE_TO_RUN_NOTIFICATION = "UNABLE TO RUN PROCESS";
  public static final String DEFAULT_DIR_PATH = "";
  static final String REVISION_HEAD = "trunk:HEAD";
  static final String CONFIG_DEFAULT = "default";
  static final String CONFIG_DEFAULT_OVERFLOW = "default--overflow";
  static final String CONFIG_VALUE_ANALYSIS_PROOF_CHECK_MULTI_EDGES_DEFAULT_PROP =
      "valueAnalysis-proofcheck-multiedges-defaultprop";
  static final String SPEC_PLAIN_TEXT = "Plain text";
  static final String SPEC_FILE = "File";
  static final String RADIO_ON = "ON";
  static final String RADIO_OFF = "OFF";
  static final String LOG_LEVEL_ALL = "All";
  static final String LOG_LEVEL_FINEST = "Finest";
  static final String LOG_LEVEL_FINER = "Finer";
  static final String LOG_LEVEL_FINE = "Fine";
  static final String LOG_LEVEL_WARNING = "Warning";
  static final String LOG_LEVEL_INFO = "Info";
  static final String LOG_LEVEL_SEVERE = "Severe";
  static final String LOG_LEVEL_OFF = "Off";
  static final String MACHINE_MODEL_LINUX32 = "Linux32";
  static final String MACHINE_MODEL_LINUX64 = "Linux64";
  static final String RADIO_LOCAL = "Local";
  static final String RADIO_CLOUD = "Cloud";
  static final String DEFAULT_PATH_LINUX = "/home";
  static final String DEFAULT_PATH_WINDOWS = "C:/";

  private ConfigurationUiConstants() {}
}
