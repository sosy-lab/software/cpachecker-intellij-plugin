package org.sosy_lab.cpachecker.plugin.intellij.execution;

import com.intellij.execution.configurations.ConfigurationFactory;
import com.intellij.execution.configurations.ConfigurationType;
import com.intellij.execution.configurations.RunConfiguration;
import com.intellij.openapi.components.BaseState;
import com.intellij.openapi.project.Project;

public class CpacheckerConfigurationFactory extends ConfigurationFactory {
  private static final String FACTORY_NAME = "CPAchecker configuration factory";

  protected CpacheckerConfigurationFactory(ConfigurationType type) {
    super(type);
  }

  @Override
  public RunConfiguration createTemplateConfiguration(Project project) {
    CpacheckerConfigurationFactory cpacheckerConfigurationFactory = this;
    return new CpacheckerRunConfiguration("CPAchecker", project, cpacheckerConfigurationFactory);
  }

  @Override
  public String getName() {
    return FACTORY_NAME;
  }

  @Override
  public Class<? extends BaseState> getOptionsClass() {
    return ConfigurationInfo.class;
  }
}
