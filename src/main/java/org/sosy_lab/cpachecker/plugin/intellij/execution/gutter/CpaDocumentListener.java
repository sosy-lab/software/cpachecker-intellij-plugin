package org.sosy_lab.cpachecker.plugin.intellij.execution.gutter;

import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.event.DocumentEvent;
import com.intellij.openapi.editor.event.DocumentListener;
import com.intellij.openapi.editor.markup.RangeHighlighter;
import com.intellij.openapi.editor.markup.TextAttributes;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiFileFactory;
import com.intellij.psi.PsiManager;
import java.util.Objects;
import org.jetbrains.annotations.NotNull;
import org.sosy_lab.cpachecker.plugin.intellij.execution.code_analysis.ObjectiveCVisitor;

public class CpaDocumentListener implements DocumentListener {
  private String fileName;
  private Editor editor;

  CpaDocumentListener(String fileName, Editor editor) {
    this.fileName = fileName;
    this.editor = editor;
  }

  @Override
  public void beforeDocumentChange(@NotNull DocumentEvent documentEvent) {}

  @Override
  public void documentChanged(@NotNull DocumentEvent documentEvent) {
    RangeHighlighter[] highlighters = editor.getMarkupModel().getAllHighlighters();
    for (RangeHighlighter highlighter : highlighters) {
      if (highlighter.getGutterIconRenderer() instanceof CpaIconRenderer) {
        editor.getMarkupModel().removeHighlighter(highlighter);
      }
    }
    Document document = editor.getDocument();
    PsiFile currentPsiFile =
        PsiManager.getInstance(Objects.requireNonNull(editor.getProject()))
            .findFile(Objects.requireNonNull(FileDocumentManager.getInstance().getFile(document)));
    String textDoc = document.getText();
    final PsiFileFactory factory = PsiFileFactory.getInstance(editor.getProject());
    final PsiFile file =
        factory.createFileFromText(Objects.requireNonNull(currentPsiFile).getLanguage(), textDoc);
    ObjectiveCVisitor objectiveCVisitor = new ObjectiveCVisitor();
    file.accept(objectiveCVisitor);
    for (int offset : objectiveCVisitor.getFunctionAndStartIndexes().values()) {
      int line = document.getLineNumber(offset);

      CpaIconRenderer cpaIconRenderer = new CpaIconRenderer(editor.getProject(), document);
      cpaIconRenderer.setLine(line);

      RangeHighlighter rangeHighlighter =
          FileListenerUtility.createRangeHighlighter(
              document, editor, line, line, new TextAttributes());
      rangeHighlighter.setGutterIconRenderer(cpaIconRenderer);
    }
  }

  String getFileName() {
    return fileName;
  }
}
