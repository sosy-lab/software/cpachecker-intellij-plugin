package org.sosy_lab.cpachecker.plugin.intellij.execution.gutter;

import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.markup.RangeHighlighter;
import com.intellij.openapi.editor.markup.TextAttributes;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.FileEditorManagerEvent;
import com.intellij.openapi.fileEditor.FileEditorManagerListener;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import org.sosy_lab.cpachecker.plugin.intellij.execution.code_analysis.ObjectiveCVisitor;

public class CpaFileEditorManagerListener implements FileEditorManagerListener {

  private List<String> openedFiles = new ArrayList<>();
  private List<CpaDocumentListener> documentListeners = new ArrayList<>();

  @Override
  public void fileClosed(FileEditorManager source, @NotNull VirtualFile file) {
    PsiFile psiFile = PsiManager.getInstance(source.getProject()).findFile(file);
    if (isObjectiveCFile(Objects.requireNonNull(psiFile))) { // check whether its a objectivec file
      String pathOldFile = getRelativePath(file.getPath());
      if (openedFiles.stream().anyMatch(f -> f.equals(pathOldFile))) {
        openedFiles.remove(pathOldFile);

        Optional<CpaDocumentListener> optionalListener =
            documentListeners.stream()
                .filter(l -> Objects.equals(l.getFileName(), pathOldFile))
                .findAny();
        optionalListener.ifPresent(
            cpaDocumentListener -> documentListeners.remove(cpaDocumentListener));
      }
    }
  }

  @Override
  public void selectionChanged(FileEditorManagerEvent event) {
    VirtualFile fileNew = event.getNewFile();
    Editor rootEditor = event.getManager().getSelectedTextEditor();
    if (rootEditor == null) {
      return;
    }

    Document rootDocument = rootEditor.getDocument();
    if (fileNew != null) {
      String lower = fileNew.getExtension();
      if (lower == null) {
        return;
      }
      PsiFile psiFile =
          PsiManager.getInstance(Objects.requireNonNull(rootEditor.getProject())).findFile(fileNew);
      if (!isObjectiveCFile(
          Objects.requireNonNull(psiFile))) { // need to check whether not a objectivec file
        return;
      }
      String pathNewFile = getRelativePath(fileNew.getPath());
      if (openedFiles.stream().noneMatch(f -> f.equals(fileNew.getPath()))) {
        openedFiles.add(fileNew.getPath());
        VirtualFile virtualFile = FileDocumentManager.getInstance().getFile(rootDocument);
        Objects.requireNonNull(virtualFile)
            .refresh(
                true,
                false,
                () -> {
                  PsiFile currentPsiFile =
                      PsiManager.getInstance(rootEditor.getProject()).findFile(virtualFile);
                  ObjectiveCVisitor objectiveCVisitor = new ObjectiveCVisitor();
                  Objects.requireNonNull(currentPsiFile).accept(objectiveCVisitor);
                  for (int offset : objectiveCVisitor.getFunctionAndStartIndexes().values()) {
                    int line = rootDocument.getLineNumber(offset);

                    CpaIconRenderer cpaIconRenderer =
                        new CpaIconRenderer(rootEditor.getProject(), rootDocument);
                    cpaIconRenderer.setLine(line);

                    RangeHighlighter rangeHighlighter =
                        FileListenerUtility.createRangeHighlighter(
                            rootDocument, rootEditor, line, line, new TextAttributes());
                    rangeHighlighter.setGutterIconRenderer(cpaIconRenderer);
                  }
                });

        CpaDocumentListener cpaDocumentListener = new CpaDocumentListener(pathNewFile, rootEditor);
        rootDocument.addDocumentListener(cpaDocumentListener);
      }
    }
  }

  private String getRelativePath(String absolutePath) {
    String base = "";
    return new File(base).toURI().relativize(new File(absolutePath).toURI()).getPath();
  }

  private boolean isObjectiveCFile(PsiFile psiFile) {
    return psiFile.getFileType().getName().toLowerCase().equals("objectivec");
  }
}
