package org.sosy_lab.cpachecker.plugin.intellij.execution;

import com.intellij.notification.NotificationType;
import com.intellij.openapi.fileChooser.FileChooser;
import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.options.SettingsEditor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ContentIterator;
import com.intellij.openapi.roots.ProjectFileIndex;
import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.ui.JBColor;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.util.ObjectUtils;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.sosy_lab.cpachecker.plugin.intellij.execution.code_analysis.ObjectiveCVisitor;
import org.sosy_lab.cpachecker.plugin.intellij.execution.notification.NotificationCreator;

public class CpaRunConfigurationSettingsEditor extends SettingsEditor<CpacheckerRunConfiguration> {
  private String[] revisionString = {ConfigurationUiConstants.REVISION_HEAD};
  private JComboBox<String> revisionCombo = new JComboBox<>(revisionString);
  private String[] specString = {
    ConfigurationUiConstants.SPEC_PLAIN_TEXT, ConfigurationUiConstants.SPEC_FILE,
  };
  private JComboBox<String> specCombo = new JComboBox(specString);
  private JTextArea specTextArea = new JTextArea();
  private JRadioButton witnessValidationOnRadioBtn =
      new JRadioButton(ConfigurationUiConstants.RADIO_ON);
  private JRadioButton witnessValidationOffRadioBtn =
      new JRadioButton(ConfigurationUiConstants.RADIO_OFF);
  private String[] logLevelString = {
    ConfigurationUiConstants.LOG_LEVEL_ALL,
    ConfigurationUiConstants.LOG_LEVEL_FINEST,
    ConfigurationUiConstants.LOG_LEVEL_FINER,
    ConfigurationUiConstants.LOG_LEVEL_FINE,
    ConfigurationUiConstants.LOG_LEVEL_INFO,
    ConfigurationUiConstants.LOG_LEVEL_WARNING,
    ConfigurationUiConstants.LOG_LEVEL_SEVERE,
    ConfigurationUiConstants.LOG_LEVEL_OFF,
  };
  private JComboBox logLevelCombo = new JComboBox(logLevelString);
  private String[] machineModelString = {
    ConfigurationUiConstants.MACHINE_MODEL_LINUX32, ConfigurationUiConstants.MACHINE_MODEL_LINUX64,
  };
  private JComboBox<String> machineModelCombo = new JComboBox(machineModelString);
  private JTextField timeLimitationField = new JTextField();
  private JTextField memoryLimitationField = new JTextField();
  private JComboBox<String> coreLimitationCombo = new JComboBox();
  private JRadioButton cloud = new JRadioButton(ConfigurationUiConstants.RADIO_CLOUD);
  private JRadioButton local = new JRadioButton(ConfigurationUiConstants.RADIO_LOCAL);
  private JFileChooser cpaDirChooser;
  private JTextField specFileChooserText = new JTextField("Choose Specification File");
  private TextFieldWithBrowseButton chooseSpecFileBtn =
      new TextFieldWithBrowseButton(specFileChooserText);
  private JTextField witnessValidationFileChooserField =
      new JTextField("Choose Witness Validation File");
  private TextFieldWithBrowseButton chooseWitnessValidationFileBtn =
      new TextFieldWithBrowseButton(witnessValidationFileChooserField);
  private JTextField chooseConfigFileTextField = new JTextField("Choose Configuration File");
  private TextFieldWithBrowseButton chooseConfigFileBtn =
      new TextFieldWithBrowseButton(chooseConfigFileTextField);
  private JTextField chooseCPADirBtnText = new JTextField("Choose CPA directory");
  private TextFieldWithBrowseButton chooseCPADirBtn =
      new TextFieldWithBrowseButton(chooseCPADirBtnText);
  private JRadioButton graficOutputOnRadioBtn = new JRadioButton(ConfigurationUiConstants.RADIO_ON);
  private JRadioButton graficOutputOffRadioBtn =
      new JRadioButton(ConfigurationUiConstants.RADIO_OFF);
  private String cpaDir = ConfigurationUiConstants.DEFAULT_DIR_PATH;
  private String specFilePath = ConfigurationUiConstants.DEFAULT_DIR_PATH;
  private ButtonGroup witnessValidationRadioGroup = new ButtonGroup();
  private ButtonGroup graphicOutputRadioGroup = new ButtonGroup();
  private ButtonGroup cloudLocalButtonGroup = new ButtonGroup();
  private String witnessValidationPath = ConfigurationUiConstants.DEFAULT_DIR_PATH;
  private String configFilePath = ConfigurationUiConstants.DEFAULT_DIR_PATH;
  private JComponent configWindow;
  private JLabel cpaCheckerRevisionLabel;
  private JLabel configurationLabel;
  private JLabel propertiesOrSpecificationLabel;
  private JLabel witnessValidation;
  private JLabel logLevelLabel;
  private JLabel machineModel;
  private JLabel timeLimitation;
  private JLabel memoryLimitation;
  private JLabel cpuCoreLimitation;
  private JLabel executionType;
  private JLabel graphicOutput;
  private JLabel srcFileForExecutionLabel;
  private JLabel functionForExecutionLabel;
  private JComboBox<String> availableSrcOCFilesCombo = new JComboBox<>();
  private JComboBox<String> availableFunctionsInSelectedFileCombo = new JComboBox<>();
  private JComponent specComponent;
  private JTextField secondTextField = new JTextField();
  private JLabel errorLabel = new JLabel();
  private String errorTextWrongCpaDir = "No valid CPAchecker directory";

  @Override
  protected void resetEditorFrom(CpacheckerRunConfiguration config) {
    revisionCombo.setSelectedItem(config.getRevisionString());
    specCombo.setSelectedItem(config.getSpecComboString());
    specTextArea.setText(config.getSpecAreaText());
    witnessValidationOnRadioBtn.setSelected(config.isWitnessValidationOn());
    if (config.isWitnessValidationOn()) {
      performOnWitnessAction();
    } else {
      performOffWitnessAction();
    }
    graficOutputOnRadioBtn.setSelected(config.isGraphicOutputOn());
    cloud.setSelected(config.isCloud());
    logLevelCombo.setSelectedItem(config.getLogLevelString());
    machineModelCombo.setSelectedItem(config.getMachineModelString());
    memoryLimitationField.setText(config.getMemoryLimitationString());
    coreLimitationCombo.setSelectedItem(config.getCoreLimitationString());
    availableSrcOCFilesCombo.setSelectedItem(config.getSrcFileForExecution());
    availableFunctionsInSelectedFileCombo.setSelectedItem(config.getFunctionForExecution());
    specFilePath = getStringRepresentation(config.getSpecFilePathString());
    if (!specFilePath.equals(ConfigurationUiConstants.DEFAULT_DIR_PATH)) {
      specFileChooserText.setText(specFilePath);
    }
    witnessValidationPath = getStringRepresentation(config.getWitnessValidationFilePathString());
    if (!witnessValidationPath.equals(ConfigurationUiConstants.DEFAULT_DIR_PATH)) {
      witnessValidationFileChooserField.setText(witnessValidationPath);
    }
    cpaDir = getStringRepresentation(config.getCpaDir());
    if (!cpaDir.equals(ConfigurationUiConstants.DEFAULT_DIR_PATH)) {
      chooseCPADirBtnText.setText(cpaDir);
      if (!isValidCpaDirectory(cpaDir)) {
        chooseCPADirBtnText.setBorder(new LineBorder(JBColor.RED, 1));
        errorLabel.setText(errorTextWrongCpaDir);
        chooseConfigFileBtn.setEnabled(false);
        chooseSpecFileBtn.setEnabled(false);
      }
    } else {
      chooseConfigFileBtn.setEnabled(false);
      chooseSpecFileBtn.setEnabled(false);
    }
    configFilePath = getStringRepresentation(config.getConfigFilePathString());
    if (!configFilePath.equals(ConfigurationUiConstants.DEFAULT_DIR_PATH)) {
      chooseConfigFileTextField.setText(configFilePath);
    }
    if (config.getTimeLimitationSeconds() != null
        && !config.getTimeLimitationSeconds().equals("")) {
      int seconds = Integer.parseInt(config.getTimeLimitationSeconds());
      secondTextField.setText(Integer.toString(seconds));
    }
    if (config.isCloud()) {
      coreLimitationCombo.setEnabled(true);
      memoryLimitationField.setEnabled(true);
      machineModelCombo.setEnabled(true);
      revisionCombo.setEnabled(true);
    } else {
      coreLimitationCombo.setEnabled(false);
      memoryLimitationField.setEnabled(false);
      machineModelCombo.setEnabled(false);
      revisionCombo.setEnabled(false);
    }
  }

  @Override
  protected void applyEditorTo(CpacheckerRunConfiguration config) throws ConfigurationException {
    if (secondTextField.getText() != null && !secondTextField.getText().equals("")) {
      String value = secondTextField.getText();
      try {
        int seconds = Integer.parseInt(value);
        config.setTimeLimitationSeconds(Integer.toString(seconds));
      } catch (NumberFormatException err) {
        NotificationCreator notificationCreator = new NotificationCreator();
        notificationCreator.createNotification(
            "Wrong configuration",
            "Please use integer value for time Limitation field",
            NotificationType.ERROR,
            CpacheckerRunConfiguration.getCurrentProject());
        secondTextField.setText("0");
      }
    } else {
      config.setTimeLimitationSeconds("");
    }

    config.setRevisionString(getStringRepresentation(revisionCombo.getSelectedItem()));
    config.setConfigFilePathString(getStringRepresentation(configFilePath));
    config.setSpecComboString(getStringRepresentation(specCombo.getSelectedItem()));
    config.setSpecAreaText(specTextArea.getText());
    if (witnessValidationRadioGroup.getSelection() != null
        && witnessValidationRadioGroup
            .getSelection()
            .getActionCommand()
            .equals(ConfigurationUiConstants.RADIO_ON)) {
      config.setWitnessValidationOn(true);
    } else {
      witnessValidationOffRadioBtn.setSelected(true);
      config.setWitnessValidationOn(false);
    }
    if (graphicOutputRadioGroup.getSelection() != null
        && graphicOutputRadioGroup
            .getSelection()
            .getActionCommand()
            .equals(ConfigurationUiConstants.RADIO_ON)) {
      config.setGraphicOutputOn(true);
    } else {
      graficOutputOffRadioBtn.setSelected(true);
      config.setGraphicOutputOn(false);
    }
    if (cloudLocalButtonGroup.getSelection() != null
        && cloudLocalButtonGroup
            .getSelection()
            .getActionCommand()
            .equals(ConfigurationUiConstants.RADIO_CLOUD)) {
      config.setCloud(true);
    } else {
      local.setSelected(true);
      config.setCloud(false);
    }
    config.setLogLevelString(getStringRepresentation(logLevelCombo.getSelectedItem()));
    config.setMachineModelString(getStringRepresentation(machineModelCombo.getSelectedItem()));
    config.setMemoryLimitationString(getStringRepresentation(memoryLimitationField.getText()));
    config.setCoreLimitationString(getStringRepresentation(coreLimitationCombo.getSelectedItem()));
    config.setCpaDir(cpaDir);
    config.setSpecFilePathString(specFilePath);
    config.setSrcFileForExecution(
        getStringRepresentation(availableSrcOCFilesCombo.getSelectedItem()));
    config.setFunctionForExecution(
        getStringRepresentation(availableFunctionsInSelectedFileCombo.getSelectedItem()));
    config.setWitnessValidationFilePathString(witnessValidationPath);
  }

  private String getStringRepresentation(final @Nullable Object pObject) {
    return ObjectUtils.notNull(pObject, "").toString();
  }

  @Override
  protected JComponent createEditor() {
    configWindow = new JPanel();
    configWindow.setLayout(new GridBagLayout());
    GridBagConstraints constraint = new GridBagConstraints();
    constraint.fill = GridBagConstraints.HORIZONTAL;
    cpaCheckerRevisionLabel = new JLabel("CPAChecker Revision");
    configurationLabel = new JLabel("Configuration");
    propertiesOrSpecificationLabel = new JLabel("Properties/Specification             ");
    witnessValidation = new JLabel("Witness Validation");
    logLevelLabel = new JLabel("Log Level");
    machineModel = new JLabel("Machine Model");
    timeLimitation = new JLabel("Time Limitation (in seconds)");
    memoryLimitation = new JLabel("Memory Limitation");
    cpuCoreLimitation = new JLabel("CPU Core Limitation");
    executionType = new JLabel("Execution Type");
    graphicOutput = new JLabel("Graphic Output");
    srcFileForExecutionLabel = new JLabel("Source file for Execution");
    functionForExecutionLabel = new JLabel("Function To Execute");
    witnessValidationOnRadioBtn.setActionCommand(ConfigurationUiConstants.RADIO_ON);
    witnessValidationOffRadioBtn.setActionCommand(ConfigurationUiConstants.LOG_LEVEL_OFF);
    graficOutputOnRadioBtn.setActionCommand(ConfigurationUiConstants.RADIO_ON);
    graficOutputOffRadioBtn.setActionCommand(ConfigurationUiConstants.RADIO_OFF);
    cloud.setActionCommand(ConfigurationUiConstants.RADIO_CLOUD);
    local.setActionCommand(ConfigurationUiConstants.RADIO_LOCAL);

    witnessValidationRadioGroup.add(witnessValidationOnRadioBtn);
    witnessValidationRadioGroup.add(witnessValidationOffRadioBtn);
    graphicOutputRadioGroup.add(graficOutputOnRadioBtn);
    graphicOutputRadioGroup.add(graficOutputOffRadioBtn);
    cloudLocalButtonGroup.add(cloud);
    cloudLocalButtonGroup.add(local);
    chooseSpecFileBtn.setEditable(false);
    chooseWitnessValidationFileBtn.setEditable(false);
    chooseCPADirBtn.setEditable(false);
    chooseConfigFileBtn.setEditable(false);
    // not implemented features disabled.
    graficOutputOnRadioBtn.setEnabled(false);

    errorLabel.setForeground(JBColor.RED);

    for (int i = 0; i < Runtime.getRuntime().availableProcessors(); i++) {
      coreLimitationCombo.addItem(Integer.toString(i + 1));
    }
    coreLimitationCombo.setSelectedItem(
        Integer.toString(Runtime.getRuntime().availableProcessors()));

    chooseSpecFileBtn.addActionListener(
        new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent actionEvent) {
            FileChooserDescriptor descriptor =
                new FileChooserDescriptor(true, false, false, false, false, false);
            VirtualFile baseFile = LocalFileSystem.getInstance().findFileByPath(specFilePath);
            if (baseFile == null) {
              baseFile =
                  LocalFileSystem.getInstance()
                      .findFileByPath(cpaDir + "/config/specification/default.spc");
            }
            final VirtualFile virtualFile =
                FileChooser.chooseFile(
                    descriptor, CpacheckerRunConfiguration.getCurrentProject(), baseFile);
            if (virtualFile != null) {
              specFilePath = virtualFile.getPath();
              specFileChooserText.setText(specFilePath);
            }
          }
        });
    chooseCPADirBtn.addActionListener(
        actionEvent -> {
          FileChooserDescriptor descriptor =
              new FileChooserDescriptor(false, true, false, false, false, false);
          final VirtualFile baseDir = LocalFileSystem.getInstance().findFileByPath(cpaDir);
          final VirtualFile virtualFile =
              FileChooser.chooseFile(
                  descriptor, CpacheckerRunConfiguration.getCurrentProject(), baseDir);
          if (virtualFile != null) {
            cpaDir = virtualFile.getPath();
            chooseCPADirBtnText.setText(cpaDir);
            if (isValidCpaDirectory(cpaDir)) {
              chooseConfigFileBtn.setEnabled(true);
              chooseSpecFileBtn.setEnabled(true);
              chooseCPADirBtnText.setBorder(new LineBorder(JBColor.WHITE, 1));
              errorLabel.setText("");
            } else {
              chooseCPADirBtnText.setBorder(new LineBorder(JBColor.RED, 1));
              errorLabel.setText(errorTextWrongCpaDir);
              chooseConfigFileBtn.setEnabled(false);
              chooseSpecFileBtn.setEnabled(false);
            }
          }
        });
    chooseWitnessValidationFileBtn.addActionListener(
        actionEvent -> {
          FileChooserDescriptor descriptor =
              new FileChooserDescriptor(true, false, false, false, false, false);
          final VirtualFile baseFile =
              LocalFileSystem.getInstance().findFileByPath(witnessValidationPath);
          final VirtualFile virtualFile =
              FileChooser.chooseFile(
                  descriptor, CpacheckerRunConfiguration.getCurrentProject(), baseFile);
          if (virtualFile != null) {
            witnessValidationPath = virtualFile.getPath();
            witnessValidationFileChooserField.setText(witnessValidationPath);
          }
        });

    chooseConfigFileBtn.addActionListener(
        actionEvent -> {
          FileChooserDescriptor descriptor =
              new FileChooserDescriptor(true, false, false, false, false, false);
          descriptor.setRoots();
          VirtualFile baseDir = LocalFileSystem.getInstance().findFileByPath(configFilePath);
          if (baseDir == null) {
            baseDir =
                LocalFileSystem.getInstance().findFileByPath(cpaDir + "/config/default.properties");
          }
          final VirtualFile virtualFile =
              FileChooser.chooseFile(
                  descriptor, CpacheckerRunConfiguration.getCurrentProject(), baseDir);
          if (virtualFile != null) {
            configFilePath = virtualFile.getPath();
            chooseConfigFileTextField.setText(configFilePath);
          }
        });

    witnessValidationOnRadioBtn.addActionListener(
        new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent actionEvent) {
            performOnWitnessAction();
          }
        });

    witnessValidationOffRadioBtn.addActionListener(
        new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent actionEvent) {
            performOffWitnessAction();
          }
        });

    cloud.addActionListener(
        new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent actionEvent) {
            if (cloud.isSelected()) {
              coreLimitationCombo.setEnabled(true);
              memoryLimitationField.setEnabled(true);
              machineModelCombo.setEnabled(true);
              revisionCombo.setEnabled(true);
            }
          }
        });

    local.addActionListener(
        new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent actionEvent) {
            if (local.isSelected()) {
              coreLimitationCombo.setEnabled(false);
              memoryLimitationField.setEnabled(false);
              machineModelCombo.setEnabled(false);
              revisionCombo.setEnabled(false);
            }
          }
        });

    addAvailableFilesToCombo();
    availableSrcOCFilesCombo.addItemListener(
        new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent itemEvent) {
            addAvailableFunctionsToCombo();
          }
        });
    specCombo.addItemListener(
        new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent itemEvent) {
            addSpecElement((specCombo.getSelectedItem().toString()));
          }
        });
    addAvailableFunctionsToCombo();
    constraint.weightx = 1.0;
    constraint.anchor = GridBagConstraints.LINE_START;
    constraint.gridx = 0;
    constraint.gridy = 0;
    constraint.gridwidth = 2;
    constraint.ipadx = 10;
    configWindow.add(cpaCheckerRevisionLabel, constraint);

    constraint.gridx = 3;
    constraint.gridy = 0;
    constraint.gridwidth = 4;
    constraint.ipadx = 4;
    configWindow.add(revisionCombo, constraint);

    constraint.gridx = 0;
    constraint.gridy = 1;
    constraint.gridwidth = 2;
    constraint.ipadx = 4;
    configWindow.add(configurationLabel, constraint);

    constraint.gridx = 3;
    constraint.gridy = 1;
    constraint.gridwidth = 4;
    constraint.ipadx = 4;
    configWindow.add(chooseConfigFileBtn, constraint);

    constraint.gridx = 0;
    constraint.gridy = 2;
    constraint.gridwidth = 2;
    constraint.ipadx = 4;
    configWindow.add(propertiesOrSpecificationLabel, constraint);

    constraint.gridx = 3;
    constraint.gridy = 2;
    constraint.gridwidth = 4;
    constraint.ipadx = 4;
    configWindow.add(specCombo, constraint);

    addSpecElement("Plain text");

    constraint.ipady = 0;
    int count = 1;
    constraint.gridx = 0;
    constraint.gridy = 3 + count;
    constraint.gridwidth = 2;
    constraint.ipadx = 4;
    configWindow.add(witnessValidation, constraint);

    constraint.gridx = 3;
    constraint.gridy = 4;
    constraint.gridwidth = 2;
    constraint.ipadx = 4;
    configWindow.add(witnessValidationOnRadioBtn, constraint);

    constraint.gridx = 5;
    constraint.gridy = 4;
    constraint.gridwidth = 1;
    constraint.ipadx = 4;
    configWindow.add(witnessValidationOffRadioBtn, constraint);

    constraint.gridx = 6;
    constraint.gridy = 4;
    constraint.gridwidth = 1;
    constraint.ipadx = 4;
    configWindow.add(chooseWitnessValidationFileBtn, constraint);

    constraint.gridx = 0;
    constraint.gridy = 4 + count;
    constraint.gridwidth = 2;
    constraint.ipadx = 4;
    configWindow.add(logLevelLabel, constraint);

    constraint.gridx = 3;
    constraint.gridy = 4 + count;
    constraint.gridwidth = 4;
    constraint.ipadx = 4;
    configWindow.add(logLevelCombo, constraint);

    constraint.gridx = 0;
    constraint.gridy = 5 + count;
    constraint.gridwidth = 2;
    constraint.ipadx = 4;
    configWindow.add(machineModel, constraint);

    constraint.gridx = 3;
    constraint.gridy = 5 + count;
    constraint.gridwidth = 4;
    constraint.ipadx = 4;
    configWindow.add(machineModelCombo, constraint);

    constraint.gridx = 0;
    constraint.gridy = 6 + count;
    constraint.gridwidth = 2;
    constraint.ipadx = 4;
    configWindow.add(timeLimitation, constraint);

    constraint.gridx = 3;
    constraint.gridy = 6 + count;
    constraint.gridwidth = 4;
    constraint.ipadx = 4;
    configWindow.add(secondTextField, constraint);

    constraint.gridx = 0;
    constraint.gridy = 7 + count;
    constraint.gridwidth = 2;
    constraint.ipadx = 4;
    configWindow.add(cpuCoreLimitation, constraint);

    constraint.gridx = 3;
    constraint.gridy = 7 + count;
    constraint.gridwidth = 4;
    constraint.ipadx = 4;
    configWindow.add(coreLimitationCombo, constraint);

    constraint.gridx = 0;
    constraint.gridy = 8 + count;
    constraint.gridwidth = 2;
    constraint.ipadx = 4;
    configWindow.add(memoryLimitation, constraint);

    constraint.gridx = 3;
    constraint.gridy = 8 + count;
    constraint.gridwidth = 4;
    constraint.ipadx = 4;
    configWindow.add(memoryLimitationField, constraint);

    constraint.gridx = 0;
    constraint.gridy = 9 + count;
    constraint.gridwidth = 2;
    constraint.ipadx = 4;
    configWindow.add(executionType, constraint);

    constraint.gridx = 3;
    constraint.gridy = 9 + count;
    constraint.gridwidth = 2;
    constraint.ipadx = 4;
    configWindow.add(cloud, constraint);

    constraint.gridx = 5;
    constraint.gridy = 9 + count;
    constraint.gridwidth = 1;
    constraint.ipadx = 4;
    configWindow.add(local, constraint);

    constraint.gridx = 6;
    constraint.gridy = 9 + count;
    constraint.gridwidth = 1;
    constraint.ipadx = 4;
    configWindow.add(chooseCPADirBtn, constraint);

    constraint.gridx = 6;
    constraint.gridy = 10 + count;
    constraint.gridwidth = 1;
    constraint.ipadx = 4;
    configWindow.add(errorLabel, constraint);

    constraint.gridx = 0;
    constraint.gridy = 10 + count;
    constraint.gridwidth = 2;
    constraint.ipadx = 4;
    configWindow.add(graphicOutput, constraint);

    constraint.gridx = 3;
    constraint.gridy = 10 + count;
    constraint.gridwidth = 2;
    constraint.ipadx = 4;
    configWindow.add(graficOutputOnRadioBtn, constraint);

    constraint.gridx = 5;
    constraint.gridy = 10 + count;
    constraint.gridwidth = 1;
    constraint.ipadx = 1;
    configWindow.add(graficOutputOffRadioBtn, constraint);

    constraint.gridx = 0;
    constraint.gridy = 11 + count;
    constraint.gridwidth = 2;
    constraint.ipadx = 4;
    configWindow.add(srcFileForExecutionLabel, constraint);

    constraint.gridx = 3;
    constraint.gridy = 11 + count;
    constraint.gridwidth = 4;
    constraint.ipadx = 4;
    configWindow.add(availableSrcOCFilesCombo, constraint);

    constraint.gridx = 0;
    constraint.gridy = 12 + count;
    constraint.gridwidth = 2;
    constraint.ipadx = 4;
    configWindow.add(functionForExecutionLabel, constraint);

    constraint.gridx = 3;
    constraint.gridy = 12 + count;
    constraint.gridwidth = 4;
    constraint.ipadx = 4;
    configWindow.add(availableFunctionsInSelectedFileCombo, constraint);

    return configWindow;
  }

  private void performOnWitnessAction() {
    GridBagConstraints constraint = new GridBagConstraints();
    constraint.fill = GridBagConstraints.HORIZONTAL;
    configWindow.remove(chooseWitnessValidationFileBtn);
    constraint.gridx = 6;
    constraint.gridy = 4;
    constraint.gridwidth = 1;
    constraint.ipadx = 4;
    configWindow.add(chooseWitnessValidationFileBtn, constraint);
    configWindow.revalidate();
    configWindow.repaint();
  }

  private void performOffWitnessAction() {
    configWindow.remove(chooseWitnessValidationFileBtn);
    configWindow.revalidate();
    configWindow.repaint();
  }

  private void addAvailableFilesToCombo() {
    Project project = CpacheckerRunConfiguration.getCurrentProject();
    if (project != null) {
      ProjectFileIndex.SERVICE
          .getInstance(project)
          .iterateContent(
              new ContentIterator() {
                @Override
                public boolean processFile(@NotNull VirtualFile fileOrDir) {
                  PsiFile psiFile = PsiManager.getInstance(project).findFile(fileOrDir);
                  if (psiFile != null) {
                    if (psiFile.getFileType().getName().toLowerCase().equals("objectivec")) {
                      System.out.println(
                          fileOrDir.getCanonicalPath()
                              + " "
                              + fileOrDir.getPath()
                              + " "
                              + project.getBasePath());
                      String fileRelativePath =
                          fileOrDir.getCanonicalPath().substring(project.getBasePath().length());
                      availableSrcOCFilesCombo.addItem(fileRelativePath);
                    }
                  }
                  return true;
                }
              });
    }
  }

  private void addAvailableFunctionsToCombo() {
    String selected = getStringRepresentation(availableSrcOCFilesCombo.getSelectedItem());
    Project thisProject = CpacheckerRunConfiguration.getCurrentProject();
    if (thisProject != null) {
      String absolutePath = thisProject.getBasePath() + selected;
      VirtualFile selectedVirtualFile = LocalFileSystem.getInstance().findFileByPath(absolutePath);
      if (selectedVirtualFile != null) {
        PsiFile selectedPsiFile = PsiManager.getInstance(thisProject).findFile(selectedVirtualFile);
        if (selectedPsiFile != null) {
          ObjectiveCVisitor objectiveCVisitor = new ObjectiveCVisitor();
          selectedPsiFile.accept(objectiveCVisitor);
          availableFunctionsInSelectedFileCombo.removeAllItems();
          for (String functionName : objectiveCVisitor.getFunctionAndStartIndexes().keySet()) {
            availableFunctionsInSelectedFileCombo.addItem(functionName);
            if (functionName.toLowerCase().equals("main")) {
              availableFunctionsInSelectedFileCombo.setSelectedItem(functionName);
            }
          }
        }
      }
    }
  }

  private void addSpecElement(String selectedItem) {
    GridBagConstraints constraint = new GridBagConstraints();
    constraint.fill = GridBagConstraints.HORIZONTAL;
    constraint.gridx = 3;
    constraint.gridy = 3;
    constraint.gridwidth = 8;
    constraint.ipadx = 4;
    constraint.ipady = 80;
    constraint.weighty = 1;
    if (selectedItem.equals(ConfigurationUiConstants.SPEC_PLAIN_TEXT)) {
      if (specComponent != null) {
        configWindow.remove(specComponent);
      }
      constraint.ipady = 0;
      constraint.fill = GridBagConstraints.BOTH;
      constraint.anchor = GridBagConstraints.FIRST_LINE_START;
      JBScrollPane sp =
          new JBScrollPane(
              specTextArea,
              JBScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
              JBScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
      sp.setMaximumSize(new Dimension(100, 20));
      configWindow.add(sp, constraint);
      specComponent = sp;
    }
    if (selectedItem.equals(ConfigurationUiConstants.SPEC_FILE)) {
      if (specComponent != null) {
        configWindow.remove(specComponent);
      }
      constraint.ipady = 0;
      constraint.weighty = 0;
      constraint.anchor = GridBagConstraints.FIRST_LINE_START;
      configWindow.add(chooseSpecFileBtn, constraint);
      specComponent = chooseSpecFileBtn;
    }
    configWindow.revalidate();
    configWindow.repaint();
  }

  private boolean isValidCpaDirectory(String path) {
    return LocalFileSystem.getInstance().findFileByPath(path + "/" + "scripts/cpa.sh") != null;
  }
}
