package org.sosy_lab.cpachecker.plugin.intellij.execution

import com.intellij.execution.configurations.RunConfigurationOptions
import com.intellij.util.xml.Attribute

open class ConfigurationInfo : RunConfigurationOptions() {

    @com.intellij.configurationStore.Property(description = "Time limit for CPAchecker execution ex: 05min 00s")
    @get:Attribute("timeLimitationSeconds")
    var timeLimitationSeconds by string("")

    @com.intellij.configurationStore.Property(description = "Revision selected from the drop down ex: trunk:HEAD")
    @get:Attribute("revisionString")
    var revisionString by string("")

    @com.intellij.configurationStore.Property(description = "Execution configuration ex: default--overflow")
    @get:Attribute("configFilePathString")
    var configFilePathString by string(ConfigurationUiConstants.DEFAULT_DIR_PATH)

    @com.intellij.configurationStore.Property(description = "Specification input modes")
    @get:Attribute("specComboString")
    var specComboString by string("")

    @com.intellij.configurationStore.Property(description = "Specification text")
    @get:Attribute("specAreaText")
    var specAreaText by string("")

    @com.intellij.configurationStore.Property(description = "Specification text")
    @get:Attribute("specFilePathString")
    var specFilePathString by string(ConfigurationUiConstants.DEFAULT_DIR_PATH)

    @com.intellij.configurationStore.Property(description = "Witness validation on or off. on?1:0")
    @get:Attribute("isWitnessValidationOn")
    var isWitnessValidationOn by property(false)

    @com.intellij.configurationStore.Property(description = "Different log levels. ex: ALL, SEVERE, OFF")
    @get:Attribute("logLevelString")
    var logLevelString by string("Info")

    @com.intellij.configurationStore.Property(description = "Machine model options")
    @get:Attribute("machineModelString")
    var machineModelString by string("")

    @com.intellij.configurationStore.Property(description = "Memory limitation ex: 2.0 GB")
    @get:Attribute("memoryLimitationString")
    var memoryLimitationString by string("")

    @com.intellij.configurationStore.Property(description = "number of core utilization limit ex: 4")
    @get:Attribute("coreLimitationString")
    var coreLimitationString by string("")

    @com.intellij.configurationStore.Property(description = "CPAchecker installation path")
    @get:Attribute("cpaDir")
    var cpaDir by string(ConfigurationUiConstants.DEFAULT_DIR_PATH)

    @com.intellij.configurationStore.Property(description = "CPAchecker exection will be run on cloud or local. 1 : cloud, 0 : local")
    @get:Attribute("isCloud")
    var isCloud by property(false)

    @com.intellij.configurationStore.Property(description = "Display graphic output at the end of execution, 1 : display, 0 : no")
    @get:Attribute("isGraphicOutputOn")
    var isGraphicOutputOn by property(false)

    @com.intellij.configurationStore.Property(description = "Selected file relative path for default execution")
    @get:Attribute("srcFileForExecution")
    var srcFileForExecution by string("")

    @com.intellij.configurationStore.Property(description = "Selected function name for the default execution")
    @get:Attribute("functionForExecution")
    var functionForExecution by string("main")

    @com.intellij.configurationStore.Property(description = "Witness validation file path")
    @get:Attribute("witnessValidationFilePathString")
    var witnessValidationFilePathString by string(ConfigurationUiConstants.DEFAULT_DIR_PATH)
}
