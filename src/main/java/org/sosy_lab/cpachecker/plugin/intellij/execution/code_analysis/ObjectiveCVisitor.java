package org.sosy_lab.cpachecker.plugin.intellij.execution.code_analysis;

import com.jetbrains.cidr.lang.psi.OCFunctionDefinition;
import com.jetbrains.cidr.lang.psi.visitors.OCRecursiveVisitor;
import java.util.HashMap;
import java.util.Map;

public class ObjectiveCVisitor extends OCRecursiveVisitor {
  private Map<String, Integer> functionAndStartIndexes = new HashMap<>();

  @Override
  public void visitFunctionDefinition(OCFunctionDefinition functionDefinition) {
    super.visitFunctionDefinition(functionDefinition);
    functionAndStartIndexes.put(
        functionDefinition.getNameIdentifier().getText(),
        functionDefinition.getStartOffsetInParent());
  }

  public Map<String, Integer> getFunctionAndStartIndexes() {
    return functionAndStartIndexes;
  }
}
