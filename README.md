# IntelliJ CPAchecker Plugin
This is a CLion plugin development project. 

# How to run
1. clone this project.
2. open it in a intellij IDE.
3. import gradle.
4. now you can run task "runIde" under gradle tasks.

# Developed features
* User can create and edit CPAchecker run configuration in the default configuration area which given in the IDE.
* User can run CPAchecker locally on the main.c file by clicking main run button on the IDE.
* User can run CPAchecker locally on functions by clicking gutter icons near to the specific functions.
* Results of the CPAchecker org.sosy_lab.cpachecker.plugin.intellij.execution will be shown using a console window.

# To run CPAchecker on a file or function first follow these steps.
1. Click on edit configuration.
2. Click '+' button to add new configuration.
3. In the drop down select CPAchecker.
4. Fill the required features as shown in the picture or configure it in your own way.
5. Click apply and ok after you configured everything.

# How to run CPAchecker
* Make sure you followed above steps.
* You can run CPA checker on main.c file by clicking run button in the IDE.
* You can run CPA checker on a function by clicking gutter icon near to the specific function.
* In both run CPAcheker will get configurations from what you specified in the CPAchecker configuration UI.